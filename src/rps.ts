export enum ThrowEnum {
    ROCK = 'rock',
    SCISSORS = 'scissors',
    PAPER = 'paper'
}

export enum ResultEnum {
    P1WINS = 'p1wins',
    P2WINS = 'p2wins',
    TIE = 'tie',
}

export class Rps {

    static play(p1: string, p2: string): string {
        if (p1 === p2) {
            return ResultEnum.TIE
        }
        if (this.isP1Wins(p1, p2)) {
            return ResultEnum.P1WINS
        }
        return ResultEnum.P2WINS
    }

    private static isP1Wins(p1: string, p2: string) {
        return p1 === ThrowEnum.ROCK && p2 === ThrowEnum.SCISSORS ||
            p1 === ThrowEnum.PAPER && p2 === ThrowEnum.ROCK ||
            p1 === ThrowEnum.SCISSORS && p2 === ThrowEnum.PAPER
    }
}