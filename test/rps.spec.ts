import {ResultEnum, Rps, ThrowEnum} from '../src/rps'

describe('rps.ts', () => {
    let rps: Rps
    beforeEach(function () {
        rps = new Rps()
    })

    it('グーがパーに負ける', () => {
        expect(Rps.play(ThrowEnum.ROCK, ThrowEnum.PAPER)).toEqual(ResultEnum.P2WINS)
    })

    it('グーがチョキに勝つ', () => {
        expect(Rps.play(ThrowEnum.ROCK, ThrowEnum.SCISSORS)).toEqual(ResultEnum.P1WINS)
    })

    it('グーとグーで引き分ける', () => {
        expect(Rps.play(ThrowEnum.ROCK, ThrowEnum.ROCK)).toEqual(ResultEnum.TIE)
    })

    it('パーがチョキに負ける', () => {
        expect(Rps.play(ThrowEnum.PAPER, ThrowEnum.SCISSORS)).toEqual(ResultEnum.P2WINS)
    })

    it('パーがグーに勝つ', () => {
        expect(Rps.play(ThrowEnum.PAPER, ThrowEnum.ROCK)).toEqual(ResultEnum.P1WINS)
    })

    it('パーとパーで引き分ける', () => {
        expect(Rps.play(ThrowEnum.PAPER, ThrowEnum.PAPER)).toEqual(ResultEnum.TIE)
    })

    it('チョキがグーに負ける', () => {
        expect(Rps.play(ThrowEnum.SCISSORS, ThrowEnum.ROCK)).toEqual(ResultEnum.P2WINS)
    })

    it('チョキがパーに勝つ', () => {
        expect(Rps.play(ThrowEnum.SCISSORS, ThrowEnum.PAPER)).toEqual(ResultEnum.P1WINS)
    })

    it('チョキとチョキで引き分ける', () => {
        expect(Rps.play(ThrowEnum.SCISSORS, ThrowEnum.SCISSORS)).toEqual(ResultEnum.TIE)
    })
})